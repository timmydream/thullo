import React from 'react';
import {
  Card as MaterialCard,
  CardContent,
  CardActions,
  Typography,
} from '@material-ui/core';
import styled from 'styled-components';

import { Button } from '@thullo/ui';

interface CardProps {
  children: React.ReactChild;
  onDelete: () => void;
}

export const Card: React.FC<CardProps> = ({ children, onDelete, ...props }) => {
  return (
    <StyledCard {...props}>
      <CardContent>
        <Typography>{children}</Typography>
      </CardContent>
      <CardActions>
        <Button size="small" color="primary" onClick={onDelete}>
          Delete
        </Button>
      </CardActions>
    </StyledCard>
  );
};

const StyledCard = styled(MaterialCard)``;
