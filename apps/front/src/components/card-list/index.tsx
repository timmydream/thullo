import React, { useState } from 'react';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import { Container, Typography, Box, TextField } from '@material-ui/core';
import styled, { css } from 'styled-components';
import { makeStyles } from '@material-ui/core/styles';

import { Button, Modal } from '@thullo/ui';
import { Card } from './card';

const getItems = (count: number, offset = 0) =>
  Array.from({ length: count }, (v, k) => k).map((k) => ({
    id: `item-${k + offset}-${new Date().getTime()}`,
    content: `item ${k + offset}`,
  }));

const reorder = (list: Array<any>, startIndex: number, endIndex: number) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

/**
 * Moves an item from one list to another list.
 */
const move = (
  source: Array<any>,
  destination: Array<any>,
  droppableSource: any,
  droppableDestination: any
) => {
  const sourceClone = Array.from(source);
  const destClone = Array.from(destination);
  const [removed] = sourceClone.splice(droppableSource.index, 1);

  destClone.splice(droppableDestination.index, 0, removed);

  const result: any = {};
  result[droppableSource.droppableId] = sourceClone;
  result[droppableDestination.droppableId] = destClone;

  return result;
};
const grid = 8;

const useStyles = makeStyles((theme) => ({
  root: {
    '& .MuiTextField-root': {
      margin: theme.spacing(2, 0),
      width: 200,
    },
  },
}));

export const CardList: React.FC = () => {
  const classes = useStyles();

  const [state, setState] = useState([
    getItems(5),
    getItems(2, 6),
    getItems(3, 8),
    getItems(1, 11),
  ]);
  const [content, setContent] = useState<string>('');

  const statuses = ['Backlog', 'In Progress', 'In Review', 'Completed'];

  const onDragEnd = (result: any) => {
    const { source, destination } = result;

    if (!destination) {
      return;
    }
    const sInd = +source.droppableId;
    const dInd = +destination.droppableId;

    if (sInd === dInd) {
      const items = reorder(state[sInd], source.index, destination.index);
      const newState: any = [...state];
      newState[sInd] = items;
      setState(newState);
    } else {
      const result = move(state[sInd], state[dInd], source, destination);
      const newState = [...state];
      newState[sInd] = result[sInd];
      newState[dInd] = result[dInd];

      setState(newState.filter((group) => group.length));
    }
  };

  return (
    <StyledContainer>
      <DragDropContext onDragEnd={onDragEnd}>
        {state.map((el, ind) => (
          <div key={ind}>
            <Droppable key={ind} droppableId={`${ind}`}>
              {(provided, snapshot) => (
                <CardsContainer
                  ref={provided.innerRef}
                  isDraggingOver={snapshot.isDraggingOver}
                  {...provided.droppableProps}
                >
                  <Typography variant="subtitle1" gutterBottom>
                    {statuses[ind]}
                  </Typography>
                  {el.map((item, index) => (
                    <Draggable
                      key={item.id}
                      draggableId={item.id}
                      index={index}
                    >
                      {(provided, snapshot) => (
                        <div
                          ref={provided.innerRef}
                          {...provided.draggableProps}
                          {...provided.dragHandleProps}
                          style={{
                            margin: '0px 0px 8px',
                            ...provided.draggableProps.style,
                          }}
                        >
                          <Card
                            onDelete={() => {
                              const newState = [...state];
                              newState[ind].splice(index, 1);
                              setState(
                                newState.filter((group) => group.length)
                              );
                            }}
                          >
                            {item.content}
                          </Card>
                        </div>
                      )}
                    </Draggable>
                  ))}
                  {provided.placeholder}
                </CardsContainer>
              )}
            </Droppable>
            <Modal
              body={(onClose) => (
                <form noValidate autoComplete="off">
                  <StyledBox className={classes.root}>
                    <Typography>New card</Typography>
                    <TextField
                      value={content}
                      onChange={(e) => setContent(e.target.value)}
                      label="Content"
                      variant="outlined"
                    />
                    <Button
                      variant="outlined"
                      onClick={() => {
                        const arr: any = [
                          ...state.slice(0, ind),
                          [...el, { id: content, content }],
                          ...state.slice(ind + 1),
                        ];
                        setState(arr);
                        setContent('');
                        onClose();
                      }}
                      disabled={!content}
                    >
                      Add
                    </Button>
                  </StyledBox>
                </form>
              )}
            >
              {(onOpen) => (
                <Button
                  color="primary"
                  onClick={() => {
                    onOpen();
                  }}
                >
                  Add another card
                </Button>
              )}
            </Modal>
          </div>
        ))}
      </DragDropContext>
    </StyledContainer>
  );
};

const StyledContainer = styled(Container)`
  display: flex;
  justify-content: center;
  background: #f6f6f6;
  border-radius: 30px;
  padding: 2rem;

  @media only screen and (max-width: 768px) {
    flex-direction: column;
    border-radius: 0;
  }
`;

const StyledBox = styled(Box)`
  display: flex;
  flex-direction: column;
`;

const CardsContainer = styled('div')<{ isDraggingOver: boolean }>`
  width: 100%;
  padding: 0;

  @media only screen and (min-width: 768px) {
    width: 250px;
    padding: 8px;

    ${({ isDraggingOver }) =>
      isDraggingOver &&
      css`
        padding: 8px;
      `}
  }
`;
