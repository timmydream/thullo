import React from 'react';
import styled from 'styled-components';

import { CardList } from './card-list';

export const App: React.FC = () => {
  return (
    <MainContainer>
      <CardList />
    </MainContainer>
  );
};

const MainContainer = styled('main')``;
