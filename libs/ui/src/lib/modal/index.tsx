import React, { useState } from 'react';
import { Modal as MaterialModal, Box } from '@material-ui/core';
import styled from 'styled-components';

interface ModalProps {
  body: (onClose: () => void) => React.ReactElement;
  children: (onOpen: () => void) => React.ReactElement;
}

export const Modal: React.FC<ModalProps> = ({ children, body, ...props }) => {
  const [open, setOpen] = useState<boolean>(false);

  const handleOpen = () => setOpen(true);

  const handleClose = () => setOpen(false);

  return (
    <>
      {children(handleOpen)}
      <StyledMaterialModal open={open} onClose={handleClose} {...props}>
        <ModalBody>{body(handleClose)}</ModalBody>
      </StyledMaterialModal>
    </>
  );
};

const StyledMaterialModal = styled(MaterialModal)`
  display: flex;
  justify-content: center;
  align-items: center;
`;

const ModalBody = styled(Box)`
  padding: 3rem 2rem;
  border-radius: 30px;
  background: white;
`;
