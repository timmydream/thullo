import React from 'react';
import styled from 'styled-components';
import { Button as MaterialButton, ButtonProps } from '@material-ui/core';

export const Button: React.FC<ButtonProps> = ({ children, ...props }) => {
  return <MaterialButton children={children} {...props} />;
};
